
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <chrono>
#include <string>

#include "RTSPStream.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

volatile bool run = true;

void int_handler(int dummy){
    run = false;
}

int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Invalid arguments" << std::endl;
        return EXIT_FAILURE;
    }

    signal(SIGINT, int_handler);

    const char *input = argv[1];
    const char *output = argv[2];

    int ret;
    RTSPStream instream;

    // Register everything
    avformat_network_init();

    std::cout << "Open Stream..." << std::endl;

    instream.open(input);
//    instream.dump_info();

    AVFormatContext* outformat_ctx = nullptr;
    avformat_alloc_output_context2(&outformat_ctx, nullptr, nullptr, output);
    if (!outformat_ctx) {
        std::cerr << "Could not create output context" << std::endl;
        exit(1);
    }

    AVStream *out_stream = avformat_new_stream(outformat_ctx, instream.get_codec());
    if (!out_stream) {
        std::cerr << "Could not create output stream" << std::endl;
        exit(1);
    }

    ret = avcodec_parameters_from_context(out_stream->codecpar, instream.get_codec_ctx());
    if (ret < 0) {
        std::cerr << "Could not copy stream parameters" << std::endl;
        exit(1);
    }
    out_stream->codecpar->codec_tag = 0;

    // open output file
    ret = avio_open(&outformat_ctx->pb, output, AVIO_FLAG_WRITE);
    if (ret < 0) {
        std::cerr << "Could not open output file" << std::endl;
        exit(1);
    }
    // write output file header
    ret = avformat_write_header(outformat_ctx, NULL);
    if (ret < 0) {
        std::cerr << "Could not write header" << std::endl;
        exit(1);
    }

    bool first = true;
    int cnt = 0;
    int icnt = 0;
    int kcnt = 0;
    auto atime = std::chrono::system_clock::now();
    AVPacket packet;

    // read frames/packets from stream
    while(run){
        if(instream.read_packet(&packet)) {
            // make sure the timestamp of the fist frame zero
            if(first){
                packet.pts = 0;
                packet.dts = 0;
                first = false;
            }

            if(packet.flags & AV_PKT_FLAG_KEY){
                std::chrono::duration<double> ctime = std::chrono::system_clock::now() - atime;
                std::cout << "KEYFRAME " << kcnt << " " << packet.pts << "/" << packet.dts << ", " << icnt
                          << " frames, " << icnt / ctime.count() << " fps" << std::endl;
                kcnt++;
                icnt = 1;
                atime = std::chrono::system_clock::now();
            }

            // rescale packet timing for output stream
            auto in_tb = instream.get_video_stream()->time_base;
            auto out_tb = out_stream->time_base;
            packet.pts = av_rescale_q_rnd(packet.pts, in_tb, out_tb,static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.dts = av_rescale_q_rnd(packet.dts, in_tb, out_tb,static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.duration = av_rescale_q(packet.duration, in_tb, out_tb);
            packet.pos = -1;

            ret = av_interleaved_write_frame(outformat_ctx, &packet);
            if(ret < 0){
                std::cerr << "Error muxing packet" << std::endl;
                break;
            }

            av_packet_unref(&packet);

            cnt++;
            icnt++;
        }
    }

    std::cerr << "Finished" << std::endl;

    avio_closep(&outformat_ctx->pb);
    avformat_free_context(outformat_ctx);

    instream.close();

    return EXIT_SUCCESS;
}
