#ifndef BEHOLDER_RTSPSTREAM_H
#define BEHOLDER_RTSPSTREAM_H

#include <string>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
}

std::string av_error_str(int err);

class RTSPStream {
public:
    RTSPStream();

    bool open(std::string uri);
    void close();

    const AVCodec *get_codec();
    AVCodecContext *get_codec_ctx(){ return codec_ctx; }
    void dump_info();
    AVStream * get_video_stream();

    bool read_packet(AVPacket *packet);
    bool writeJPEG(AVFrame *frame, std::string filename);

private:
    AVDictionary *options;
    AVFormatContext* informat_ctx;
    AVCodecContext* codec_ctx;
    std::string uri;
    int video_stream_index;
};


#endif //BEHOLDER_RTSPSTREAM_H
