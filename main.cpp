 
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <chrono>
#include <thread>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswscale/swscale.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
}



int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Invalid arguments" << std::endl;
        return EXIT_FAILURE;
    }

    const char *input = argv[1];
    const char *output = argv[2];

    int ret;

    // Register everything
    avformat_network_init();

    // open RTSP stream
    AVDictionary *options = nullptr;
    av_dict_set(&options, "rtsp_transport", "tcp", 0);

    AVFormatContext* informat_ctx = avformat_alloc_context();
    ret = avformat_open_input(&informat_ctx, input, nullptr, &options);
    if (ret < 0) {
        std::cerr << "Could not open stream" << std::endl;
        return EXIT_FAILURE;
    }
    av_dict_free(&options);
    ret = avformat_find_stream_info(informat_ctx, nullptr);
    if (ret < 0) {
        std::cerr << "Could not find stream info" << std::endl;
        return EXIT_FAILURE;
    }

//    av_dump_format(informat_ctx, 0, input, 0);

    // find video stream id
    int video_stream_index = -1;
    for (unsigned i = 0; i < informat_ctx->nb_streams; i++) {
        if (informat_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
            video_stream_index = i;
    }
    if (video_stream_index < 0) {
        std::cerr << "Video stream not found" << std::endl;
        exit(1);
    }

    //open output file
    AVFormatContext* outformat_ctx = nullptr;
    avformat_alloc_output_context2(&outformat_ctx, nullptr, nullptr, output);
    if (!outformat_ctx) {
        std::cerr << "Could not create output context" << std::endl;
        exit(1);
    }

    AVStream *in_stream = informat_ctx->streams[video_stream_index];
    AVCodecContext* incodec_ctx = avcodec_alloc_context3(NULL);
    avcodec_parameters_to_context(incodec_ctx, in_stream->codecpar);
    AVStream *out_stream = avformat_new_stream(outformat_ctx, incodec_ctx->codec);
    if (!out_stream) {
        std::cerr << "Could not create output stream" << std::endl;
        exit(1);
    }

    incodec_ctx->flags = AV_CODEC_FLAG_GLOBAL_HEADER;
    ret = avcodec_parameters_from_context(out_stream->codecpar, incodec_ctx);
    if (ret < 0) {
        std::cerr << "Could not copy stream parameters" << std::endl;
        exit(1);
    }
    out_stream->codecpar->codec_tag = 0;

    ret = avio_open(&outformat_ctx->pb, output, AVIO_FLAG_WRITE);
    if (ret < 0) {
        std::cerr << "Could not open output file" << std::endl;
        exit(1);
    }
    // write output file header
    ret = avformat_write_header(outformat_ctx, NULL);
    if (ret < 0) {
        std::cerr << "Could not write header" << std::endl;
        exit(1);
    }

    /*
    //start reading packets from stream and write them to file
    av_read_play(informat_ctx);    //play RTSP

    // Get the h264 codec
    AVCodec *codec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!codec) {
        std::cerr << "H264 codec not found" << std::endl;
        exit(1);
    }

    // Add this to allocate the context by codec
    AVCodecContext* codec_ctx = avcodec_alloc_context3(codec);
    avcodec_get_context_defaults3(codec_ctx, codec);
//    avcodec_copy_context(codec_ctx, format_ctx->streams[video_stream_index]->codec);
    avcodec_parameters_to_context(codec_ctx, informat_ctx->streams[video_stream_index]->codecpar);

    std::ofstream output_file;

    if (avcodec_open2(codec_ctx, codec, NULL) < 0)
        exit(1);

    SwsContext *img_convert_ctx = sws_getContext(codec_ctx->width, codec_ctx->height, codec_ctx->pix_fmt, codec_ctx->width, codec_ctx->height, AV_PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, NULL);

    int size = avpicture_get_size(AV_PIX_FMT_YUV420P, codec_ctx->width, codec_ctx->height);
    uint8_t* picture_buffer = (uint8_t*) (av_malloc(size));
    AVFrame* picture = av_frame_alloc();
    AVFrame* picture_rgb = av_frame_alloc();
    int size2 = avpicture_get_size(AV_PIX_FMT_RGB24, codec_ctx->width, codec_ctx->height);
    uint8_t* picture_buffer_2 = (uint8_t*) (av_malloc(size2));
    avpicture_fill((AVPicture *) picture, picture_buffer, AV_PIX_FMT_YUV420P, codec_ctx->width, codec_ctx->height);
    avpicture_fill((AVPicture *) picture_rgb, picture_buffer_2, AV_PIX_FMT_RGB24, codec_ctx->width, codec_ctx->height);

    AVStream* stream = nullptr;
    */

    bool first = true;
    int cnt = 0;
    int icnt = 0;
    auto atime = std::chrono::system_clock::now();
    AVPacket packet;

    // read frames/packets from stream
    while (av_read_frame(informat_ctx, &packet) >= 0) {
        // only decode video packets
        if (packet.stream_index == video_stream_index) {
            if (packet.flags & AV_PKT_FLAG_KEY) {
                std::chrono::duration<double> ctime = std::chrono::system_clock::now() - atime;
                std::cout << "KEYFRAME " << packet.pts << "/" << packet.dts << ", " << icnt << " frames, " << icnt / ctime.count() << " fps" << std::endl;

                icnt = 0;
                atime = std::chrono::system_clock::now();
            } else {
//                std::cout << "FRAME " << packet.pts << "/" << packet.dts << std::endl;
            }
            if (packet.flags & AV_PKT_FLAG_CORRUPT) {
                std::cout << "CORRUPT" << std::endl;
            }

            // make sure the timestamp of the fist frame is int min
            if (first) {
                packet.pts = INT64_MIN;
                packet.dts = INT64_MIN;
                first = false;
            }

            // rescale packet timing for output stream
            packet.pts = av_rescale_q_rnd(packet.pts, in_stream->time_base, out_stream->time_base, static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.dts = av_rescale_q_rnd(packet.dts, in_stream->time_base, out_stream->time_base, static_cast<AVRounding>(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            packet.duration = av_rescale_q(packet.duration, in_stream->time_base, out_stream->time_base);
            packet.pos = -1;

            ret = av_interleaved_write_frame(outformat_ctx, &packet);
            if (ret < 0) {
                std::cerr << "Error muxing packet" << std::endl;
                break;
            }

            /*
            std::cout << "2 Is Video" << std::endl;
            if (stream == NULL) {    //create stream in file
                std::cout << "3 create stream" << std::endl;
                stream = avformat_new_stream(outformat_ctx, informat_ctx->streams[video_stream_index]->codec->codec);
                avcodec_copy_context(stream->codec, informat_ctx->streams[video_stream_index]->codec);
                stream->sample_aspect_ratio = informat_ctx->streams[video_stream_index]->codec->sample_aspect_ratio;
            }
            int check = 0;
            packet.stream_index = stream->id;
            std::cout << "4 decoding" << std::endl;
            int result = avcodec_decode_video2(codec_ctx, picture, &check, &packet);
            std::cout << "Bytes decoded " << result << " check " << check << std::endl;
            if (cnt > 100) {    //cnt < 0)
                sws_scale(img_convert_ctx, picture->data, picture->linesize, 0, codec_ctx->height, picture_rgb->data, picture_rgb->linesize);
                std::stringstream file_name;
                file_name << "test" << cnt << ".ppm";
                output_file.open(file_name.str().c_str());
                output_file << "P3 " << codec_ctx->width << " " << codec_ctx->height << " 255\n";
                for (int y = 0; y < codec_ctx->height; y++) {
                    for (int x = 0; x < codec_ctx->width * 3; x++)
                        output_file << (int) (picture_rgb->data[0] + y * picture_rgb->linesize[0])[x] << " ";
                }
                output_file.close();
            }
            */

            cnt++;
            icnt++;
        }

        av_packet_unref(&packet);
    }

//    av_free(picture);
//    av_free(picture_rgb);
//    av_free(picture_buffer);
//    av_free(picture_buffer_2);

    avio_closep(&outformat_ctx->pb);
    avformat_free_context(outformat_ctx);

    av_read_pause(informat_ctx);
    avformat_close_input(&informat_ctx);

    return (EXIT_SUCCESS);
}
