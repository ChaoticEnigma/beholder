
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <chrono>
#include <string>

#include "RTSPStream.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

volatile bool run = true;

void int_handler(int dummy){
    run = false;
}

int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Invalid arguments" << std::endl;
        return EXIT_FAILURE;
    }

    signal(SIGINT, int_handler);

    const char *input = argv[1];
    const char *output = argv[2];

    int ret;
    RTSPStream instream;

    // Register everything
    avformat_network_init();

    std::cout << "Open Stream..." << std::endl;

    instream.open(input);
    instream.dump_info();

    bool first = true;
    int kcnt = 0;
    auto atime = std::chrono::system_clock::now();
    AVPacket packet;

    // read frames/packets from stream
    while(run){
        if(instream.read_packet(&packet)) {
            // make sure the timestamp of the fist frame zero
            if(first){
                packet.pts = 0;
                packet.dts = 0;
                first = false;
            }

            if(packet.flags & AV_PKT_FLAG_KEY){
                auto fname = std::string(output) + std::to_string(kcnt) + ".jpg";

                atime = std::chrono::system_clock::now();

                // decode h264 keyframe
                AVCodec *dcodec = avcodec_find_decoder(AV_CODEC_ID_H264);
                AVCodecContext *dctx = avcodec_alloc_context3(dcodec);
                avcodec_open2(dctx, NULL, 0);

                AVFrame *frame = av_frame_alloc();

                int nres = avcodec_send_packet(dctx, &packet);
                if(nres >= 0){
                    nres = avcodec_receive_frame(dctx, frame);
                    if(nres >= 0){
                        // decode time
                        std::chrono::duration<double, std::milli> dtime = std::chrono::system_clock::now() - atime;
                        atime = std::chrono::system_clock::now();


                        if(!instream.writeJPEG(frame, fname)){
                            std::cout << "write jpg failed" << std::endl;
                        }

                        std::chrono::duration<double, std::milli> etime = std::chrono::system_clock::now() - atime;
                        atime = std::chrono::system_clock::now();

                        const int w = instream.get_codec_ctx()->width;
                        const int h = instream.get_codec_ctx()->height;
                        const auto fmt = (AVPixelFormat)frame->format;
                        auto fmt2 = AV_PIX_FMT_RGB24;

                        // allocate image
                        AVFrame *framergb = av_frame_alloc();
                        int sz = av_image_alloc(framergb->data, framergb->linesize, w, h, fmt2, 16);
                        if(sz > 0) {
                            // convert frame to packed RGB
                            SwsContext *sctx = sws_getContext(w, h, fmt, w, h, fmt2, SWS_BILINEAR, NULL, NULL, NULL);
                            sws_scale(sctx, (const unsigned char *const *) frame->data, frame->linesize, 0, h, framergb->data, framergb->linesize);

//                            FILE *file = fopen("test.rgb", "wb");
//                            fwrite(framergb->data[0], 1, sz, file);
//                            fclose(file);

                            av_freep(&framergb->data[0]);
                        }
                        av_frame_free(&framergb);

                        std::chrono::duration<double, std::milli> stime = std::chrono::system_clock::now() - atime;

                        std::cout << "KEYFRAME " << kcnt << " " << packet.pts << "/" << packet.dts <<
                                  " D: " << dtime.count() <<
                                  " E: " << etime.count() <<
                                  " S: " << stime.count() <<
                                  " -> " << fname << std::endl;

                    } else {
                        std::cout << "receive frame failed: " << av_error_str(nres) << std::endl;
                    }
                } else {
                    std::cout << "send packet failed: " << av_error_str(nres) << std::endl;
                }

                av_frame_free(&frame);

                kcnt++;
            }

            av_packet_unref(&packet);
        }
    }

    std::cerr << "Finished" << std::endl;

    instream.close();

    return EXIT_SUCCESS;
}
