#include "RTSPStream.h"

#include <iostream>

std::string av_error_str(int err){
    char s[100];
    av_strerror(err, s, 100);
    return std::string(s, 100);
}

RTSPStream::RTSPStream(){

}

bool RTSPStream::open(std::string input){
    uri = input;

    options = nullptr;
    informat_ctx = avformat_alloc_context();

    // open RTSP stream
    av_dict_set(&options, "rtsp_transport", "tcp", 0);

    int ret = avformat_open_input(&informat_ctx, uri.c_str(), nullptr, &options);
    if (ret < 0) {
        std::cerr << "Could not open stream" << std::endl;
        return EXIT_FAILURE;
    }

//    av_dict_free(&options);

    ret = avformat_find_stream_info(informat_ctx, nullptr);
    if (ret < 0) {
        std::cerr << "Could not find stream info" << std::endl;
        return EXIT_FAILURE;
    }

    // find video stream id
    video_stream_index = -1;
    for (unsigned i = 0; i < informat_ctx->nb_streams; i++) {
        if (informat_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
            video_stream_index = i;
    }
    if (video_stream_index < 0) {
        std::cerr << "Video stream not found" << std::endl;
        exit(1);
    }

    AVStream *in_stream = get_video_stream();
    codec_ctx = avcodec_alloc_context3(NULL);
    avcodec_parameters_to_context(codec_ctx, in_stream->codecpar);

    codec_ctx->flags = AV_CODEC_FLAG_GLOBAL_HEADER;

    return true;
}

void RTSPStream::close() {
    av_read_pause(informat_ctx);
    avformat_close_input(&informat_ctx);
    av_dict_free(&options);
}

const AVCodec *RTSPStream::get_codec() {
    return codec_ctx->codec;
}

void RTSPStream::dump_info(){
    // dump stream info
    av_dump_format(informat_ctx, 0, uri.c_str(), 0);
}

AVStream * RTSPStream::get_video_stream() {
    AVStream *in_stream = informat_ctx->streams[video_stream_index];
    return in_stream;
}

bool RTSPStream::read_packet(AVPacket *packet) {
    if(av_read_frame(informat_ctx, packet) >= 0) {
        // discard corrupt packets
        if(packet->flags & AV_PKT_FLAG_CORRUPT){
            std::cout << "CORRUPT PACKET" << std::endl;
            av_packet_unref(packet);
            return false;
        }
        // only process video packets
        if (packet->stream_index != video_stream_index) {
            av_packet_unref(packet);
            return false;
        }
        return true;
    }
    return false;
}

bool RTSPStream::writeJPEG(AVFrame *pFrame, std::string filename){
    AVCodecContext *out_codec_ctx;
    AVCodec *pOCodec;
    FILE *file;
    int Quality = 1;

    // Allocate the output codec context
    out_codec_ctx = avcodec_alloc_context3(NULL);
    if(!out_codec_ctx){
        return false;
    }
    // Initialise format parameters
    out_codec_ctx->bit_rate      = codec_ctx->bit_rate;
    out_codec_ctx->width         = codec_ctx->width;
    out_codec_ctx->height        = codec_ctx->height;
    out_codec_ctx->pix_fmt       = AV_PIX_FMT_YUVJ420P;
    out_codec_ctx->codec_id      = AV_CODEC_ID_MJPEG;
    out_codec_ctx->codec_type    = AVMEDIA_TYPE_VIDEO;
//    out_codec_ctx->time_base.num = codec_ctx->time_base.num;
//    out_codec_ctx->time_base.den = codec_ctx->time_base.den;
    out_codec_ctx->time_base.num = 1;
    out_codec_ctx->time_base.den = 25;
    // Allocate the JPEG encoder
    pOCodec = avcodec_find_encoder (out_codec_ctx->codec_id );
    if ( !pOCodec ) {
        return false;
    }
    // Open the JPEG encoder
    int r = avcodec_open2 (out_codec_ctx, pOCodec, nullptr );
    if (r < 0 ) {
        char c[100];
        av_strerror(r, c, 100);
        printf("%s\n", c);
        return false;
    }
    // Initialise the "VBR" settings
    out_codec_ctx->qmin           = out_codec_ctx->qmax = Quality;
//    out_codec_ctx->mb_lmin        = out_codec_ctx->lmin = out_codec_ctx->qmin * FF_QP2LAMBDA;
    out_codec_ctx->mb_lmin        = out_codec_ctx->qmin * FF_QP2LAMBDA;
//    out_codec_ctx->mb_lmax        = out_codec_ctx->lmax = out_codec_ctx->qmax * FF_QP2LAMBDA;
    out_codec_ctx->mb_lmax        = out_codec_ctx->qmax * FF_QP2LAMBDA;
    out_codec_ctx->flags          = AV_CODEC_FLAG_QSCALE;
    out_codec_ctx->global_quality = out_codec_ctx->qmin * FF_QP2LAMBDA;
    // Set the timestamp and quality parameters
    pFrame->pts = 1;
    pFrame->quality = out_codec_ctx->global_quality;

    AVPacket pkt;
    av_init_packet(&pkt);
    pkt.data = nullptr;
    pkt.size = 0;

    int nres = avcodec_send_frame(out_codec_ctx, pFrame);
    if(nres >= 0){
        nres = avcodec_receive_packet(out_codec_ctx, &pkt);
        if(nres >= 0){

            // Write JPEG to file
            file = fopen(filename.c_str(), "wb");
            fwrite(pkt.data, 1, pkt.size, file);
            fclose(file);
            av_packet_unref(&pkt);

        } else {
            std::cout << "receive packet failed: " << av_error_str(nres) << std::endl;
        }
    } else {
        std::cout << "send frame failed: " << av_error_str(nres) << std::endl;
    }

    avcodec_close (out_codec_ctx );

    return (nres >= 0);
}
